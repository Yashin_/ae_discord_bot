const fs = require('fs');
const Discord = require('discord.js');
const configContent = fs.readFileSync('./config.json');
var config = JSON.parse(configContent);

const client = new Discord.Client();
var commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

console.log(commandFiles);

for (const file of commandFiles) 
{
    const command = require(`./commands/${file}`);
    commands.set(command.name, command);
}
console.log(commands);

const cooldowns = new Discord.Collection();

client.once('ready', () =>      
{
    console.log('Up and running!');
});

client.on('message', message =>     
{
    console.log(message.content);
    if (!message.content.startsWith(config.prefix) || message.author.bot) return;

    const args = message.content.slice(config.prefix.length + 1).split(/ +/);
    const commandName = args.shift().toLowerCase();
    
    const command = commands.get(commandName)
        || commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
    if (!command) 
    {
        return message.reply(`Dieser Command existiert nicht! Versuch \"\`${config.prefix}\` hilfe\" für mehr Informationen.`);
    }

    if (command.guildOnly && message.channel.type !== 'text') 
    {
        return message.reply('I can\'t execute that command inside DMs!');
    }

    if (command.args && !args.length) 
    {
        let reply = `You didn't provide any arguments, ${message.author}!`;

        if (command.usage) 
        {
            reply += `\nThe proper usage would be: \`${config.prefix} ${command.name} ${command.usage}\``;
        }

        return message.channel.send(reply);
    }

    if (!cooldowns.has(command.name)) 
    {
        cooldowns.set(command.name, new Discord.Collection());
    }
    
    const now = Date.now();
    const timestamps = cooldowns.get(command.name);
    const cooldownAmount = (command.cooldown || 3) * 1000;
    
    if (timestamps.has(message.author.id)) 
    {
        const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

        if (now < expirationTime) 
        {
            const timeLeft = (expirationTime - now) / 1000;
            return message.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`);
        }
    }

    timestamps.set(message.author.id, now);
    setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

    try 
    {
        command.execute(message, args);
    }
    catch (error) 
    {
        console.error(error);
        message.reply('there was an error trying to execute that command!');
    }
});

process.on('SIGTERM', () => {
    console.info('SIGTERM signal received.');
    const wichteln_data = fs.readFileSync('./db/wichteln.json');
    fs.writeFileSync('./db_bak/wichteln.json', wichteln_data);
    process.exit(0);
});

client.login(config.discord_token);
