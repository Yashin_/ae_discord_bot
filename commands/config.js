module.exports =
{
    name: 'config',
    description: 'Bot Einstellungen.',
    args: true,
    usage: '<key> query|<value>',
    execute(message, args)
    {
        if (message.member.hasPermission("ADMINISTRATOR"))
        {
            console.log(args);
            const fs = require('fs');
            const configPath = './config.json';
            var configContent = fs.readFileSync(configPath);
            var config = JSON.parse(configContent);
            if (args.length >= 2)
            {
                if (args[0] == 'config_role')
                {
                    if (args[1] == 'query')
                    {

                    }
                    else if (args[1].startsWith('<@&'))
                    {
                        config.config_role = args[1].substring(3, args[1].length - 1);
                        fs.writeFile(configPath, JSON.stringify(config, null, 4), (err) => {
                            if (err) {
                                console.error(err);
                                return;
                            };
                            console.log("File has been created.");
                        })
                    }
                    else
                    {
                        message.channel.send('Du hast als key keine Rolle angegeben.');
                    }
                }
            }
            else
            {
                message.channel.send('Du hast nicht genügend Argumente angegeben.')
            }
        }
        else
        {
            message.channel.send('Du hast keine Berechtigung die Servereinstellungen zu ändern.');
        }
    }
}