module.exports =
{
    name: 'wichteln',
    description: 'Wichteln organisieren',
    args: true,
    usage: '<zusatzinfo>',
    execute(message, args)
    {
        const configPath = './config.json'
        const filePath = './db/wichteln.json';
        const shuffle = require("shuffle-array");
        var fs = require('fs');
        var configContent = fs.readFileSync(configPath);
        var config = JSON.parse(configContent);
        if (!fs.existsSync(filePath))
        {
            fs.writeFileSync(filePath, JSON.stringify({active: false}, null, 4));
            console.log('Saved!');
        }
        var wichteln_content = fs.readFileSync(filePath);
        var wichteln = JSON.parse(wichteln_content);
        if (args.length > 2 && args[0] == 'neu')
        {
            if (message.member.highestRole.comparePositionTo(message.guild.roles.array().find(x => x.id == config.config_role)) >= 0)
            {
                console.log('Updating');
                var description = args[7];
                for (var i = 8; i < args.length; ++i)
                {
                    description = description.concat(' ').concat(args[i]);
                }
                wichteln = {
                    active: true,
                    date: new Date(parseInt(args[2]), parseInt(args[3] - 1), parseInt(args[4]), parseInt(args[5]), parseInt(args[6])),
                    description: description,
                    role: args[1].substr(3, (args[1]).length - 4),
                    participants: [],
                    closed: false
                }
                console.log(wichteln.date.getFullYear());
                console.log(JSON.stringify(wichteln, null, 4));
                fs.writeFileSync(filePath, JSON.stringify(wichteln, null, 4));
                console.log('Saved!');
                message.channel.send(`Eine neue Wichteln Instanz wurde von ${message.author} erstellt.`);
            }
            else if (wichteln.active == true)
            {
                message.channel.send(`Es wird gerade schon gewichtelt, du kannst keine neue Instanz erstellen. Nutze \`${config.prefix} wichteln neu_ueberschreiben\` um... naja die alte Instanz zu überschreiben.`);
            }
            else
            {
                message.channel.send(`${message.author}, nur Admins dürfen eine neue Wicheln-Instanz erstellen.`);
            }
        }
        else if (args.length > 2 && args[0] == 'neu_ueberschreiben')
        {
            if (message.member.highestRole.comparePositionTo(message.guild.roles.array().find(x => x.id == config.config_role)) >= 0)
            {
                if (wichteln.active)
                {
                    message.channel.send(`Alte Wichteln-instanz wird ueberschrieben.`)
                }
                console.log('Updating');
                var date = args[2];
                var description = args[7];
                for (var i = 8; i < args.length; ++i)
                {
                    description = description.concat(' ').concat(args[i]);
                }
                console.log('role'.concat(args[1]));
                wichteln = {
                    active: true,
                    date: new Date(parseInt(args[2]), parseInt(args[3] - 1), parseInt(args[4]), parseInt(args[5]), parseInt(args[6])),
                    description: description,
                    role: args[1].substr(3, (args[1]).length - 4),
                    participants: [],
                    closed: false
                }
                console.log(wichteln.date.getFullYear());
                console.log(JSON.stringify(wichteln, null, 4));
                fs.writeFileSync(filePath, JSON.stringify(wichteln, null, 4));
                console.log('Saved!');
                message.channel.send(`Eine neue Wichteln Instanz wurde von ${message.author} erstellt.`);
            }
            else
            {
                message.channel.send(`${message.author}, nur Admins dürfen eine neue Wicheln-Instanz erstellen.`);
            }
        }
        else if (!wichteln.active)
        {
            message.channel.send('Es besteht zurzeit keine Wichteln-Instanz. Wende dich an die Admins wegen Hilfe.');
        }
        else if (args.length > 0 && args[0] == "eintragen")
        {
            if (args.length > 1)
            {
                if (message.member.highestRole.comparePositionTo(message.guild.roles.array().find(x => x.id == wichteln.role)) >= 0)
                {
                    console.log("eintragen");
                    if (!wichteln.closed)
                    {
                        var eintrag = {
                            id: '',
                            name: '',
                            text: '',
                            partner_id: ''
                        };
                        eintrag.text = args[1];
                        for (var i = 2; i < args.length; ++i)
                        {
                            eintrag.text = eintrag.text.concat(' ').concat(args[i]);
                        }
                        eintrag.id = message.member.id;
                        eintrag.name = message.member.displayName;
                        var index = wichteln.participants.findIndex(x => x.id == eintrag.id);
                        if (index == -1)
                        {
                            wichteln.participants.push(eintrag);
                            fs.writeFileSync(filePath, JSON.stringify(wichteln, null, 4));
                            console.log('Saved!');
                            message.channel.send('Du hast dich fuer das Wichteln eingetragen.');
                        }
                        else
                        {
                            wichteln.participants[index] = eintrag;
                            fs.writeFileSync(filePath, JSON.stringify(wichteln, null, 4));
                            console.log('Saved!');
                            message.channel.send('Du hast deinen Eintrag fuer das Wichteln abgeaendert.');
                        }
                    }
                    else
                    {
                        message.channel.send('Die Anmeldung fuer diese Wichteln-Instanz wurde bereits geschlossen.');
                    }
                }
                else
                {
                    message.channel.send(`Du darfst nicht an diesem Wichteln teilnehmen. Du musst mindestens die Rolle <@&${wichteln.role}> haben um teilzunehmen.`);
                }
            }
            else
            {
                message.channel.send(`Du hast keine Nachricht zu deinem Eintrag gegeben. Versuch es mit \`${config.prefix} wichteln <Nachricht>\`.`);
            }
        }
        else if (args.length > 0 && args[0] == 'close')
        {
            if (message.member.highestRole.comparePositionTo(message.guild.roles.array().find(x => x.id == config.config_role)) >= 0)
            {
                console.log('closing');
                wichteln.closed = true;
                var ids = wichteln.participants.map(x => x.id);
                var ids_shuffle = shuffle(ids, {'copy': true});
                var i = 0;
                while (true)
                {
                    if (i == ids_shuffle.length)
                    {
                        break;
                    }
                    if (ids_shuffle[i] == ids[i++])
                    {
                        ids_shuffle = shuffle(ids, {'copy': true});
                        i = 0;
                    }
                }
                for (var i = 0; i < ids.length; ++i)
                {
                    console.log(`set ${i}`);
                    wichteln.participants[i].partner_id = ids_shuffle[i];
                }
                fs.writeFileSync(filePath, JSON.stringify(wichteln, null, 4));
                console.log('Saved!');
                message.channel.send('Die Wichteln Instanz wurde geschlossen.');
                for (var i = 0; i < wichteln.participants.length; ++i)
                {
                    message.guild.members.array().find(x => x.id == wichteln.participants[i].id).send(`Dein Partner ist: ${wichteln.participants.find(x => x.id == wichteln.participants[i].partner_id).name}.`);
                }
            }
            else
            {
                message.channel.send('Du hast keine Berechtigungen die Anmeldephase zu schließen.');
            }
        }
        else if (args.length > 0 && args[0] == 'partner')
        {
            if (args.length == 1)
            {
                if (message.member.highestRole.comparePositionTo(message.guild.roles.array().find(x => x.id == wichteln.role)) >= 0)
                {
                    var result = wichteln.participants.find(x => x.id == message.member.id);
                    var partner = wichteln.participants.find(x => x.id == result.partner_id);
                    var revPartner = wichteln.participants.find(x => x.partner_id == result.id);
                    if (wichteln.closed && new Date(wichteln.date).getTime() < Date.now())
                    {
                        message.author.send(`Dein Wichtelpartner ist: ${partner.name}. Du bekommst ein Geschenk von ${revPartner.name}.`);
                    }
                    else if (wichteln.closed)
                    {
                        message.author.send(`Dein Wichtelpartner ist: ${partner.name}.`);
                    }
                    else if (wichteln.active)
                    {
                        message.author.send(`Die Anmeldephase ist noch nicht vorbei und die Partner wurden noch nicht ausgelost.`);
                    }
                    else
                    {
                        message.author.send(`Es wird gerade nicht gewichtelt.`);
                    }
                }
            }
            else if (args[1] == "list")
            {
                if (message.member.highestRole.comparePositionTo(message.guild.roles.array().find(x => x.id == config.config_role)) >= 0 && wichteln.active && !wichteln.closed)
                {
                    message.author.send(wichteln.participants.map(x => x.name.concat(':').concat(wichteln.participants.find(y => y.id == x.partner_id))));
                }
            }
            else
            {
                if (wichteln.active)
                {
                    if (wichteln.closed)
                    {
                        if (message.member.highestRole.comparePositionTo(message.guild.roles.array().find(x => x.id == config.config_role)) >= 0)
                        {
                            var result = wichteln.participants.find(x => x.name == args[1] || x.id == args[1]);
                            var partner = wichteln.participants.find(x => x.id == result.partner_id);
                            var revPartner = wichteln.participants.find(x => x.partner_id == result.id);
                            message.author.send(`Der Partner von ${result.name} ist: ${partner.name}. ${result.name} wird von ${revPartner.name} beschenkt.`);
                        }
                        else
                        {
                            message.channel.send('Du hast keine Berechtigung die Partner anderer Leute einzusehen.');
                        }
                    }
                    else
                    {
                        message.channel.send('Die Anmeldephase ist noch nicht vorbei.')
                    }
                }
                else
                {
                    message.channel.send('Es wird gerade nicht gewichtelt.');
                }
            }
        }
    }
}
